#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>

#define PORT 8080
#define IP_PROTOCOL 0
#define MAX_PENDING_CONNECTION 5
#define REQ_LEN 100
#define RESP_LEN 2000

void *connection_handler(void *socketPtr);
void command_handler(int socket, char *cmd);

int register_user(int socket, char *arg);
int login_user(int socket, char *arg);

bool startsWith(const char *pre, const char *str);
char *trim_whitespace(char *str);
int write_user_info(char *user, char *pass);
int valid_user(char* user, int len);
int valid_pass(char* pass, int len);
int login_check(char* user, char* pass);
void removeSubstr (char *string, char *sub);


int main(int argc, char const *argv[]) {
	int serverSocket, clientSocket, *clientSocketPtr	;
	struct sockaddr_in serverAddr, clientAddr;
	int sockaddrLen = sizeof(struct sockaddr_in);
	int option = 1;
	char *response, clientReq[2000];

	// create server socket for IPv4, TCP, and IP Protocol
	if ((serverSocket = socket(AF_INET, SOCK_STREAM, IP_PROTOCOL)) == 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	
	// bind server socket
	if (bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) {
		perror("socket bind failed");
		exit(EXIT_FAILURE);
	}

	// set server socket to listen for connection
	if (listen(serverSocket, MAX_PENDING_CONNECTION) < 0) {
		perror("socket listen failed");
		exit(EXIT_FAILURE);
	}

	puts("Server is running...");

	// continuously accept connection from client socket
	while(clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddr, (socklen_t*) &sockaddrLen)) {
		printf("Connection accepted\n");

		// response to client connection
		response = "Hello Client, I have received your connection. And now I will assign a handler for you";
		write(clientSocket, response , strlen(response));
		printf("Response sent\n");
		printf("Response content : %s\n", response);

		// create socketHandler with thread
		pthread_t socketHandler;
		clientSocketPtr =  malloc(1);
		*clientSocketPtr = clientSocket;

		// assign socketHandler to each client socket connected
		if(pthread_create(&socketHandler, NULL, connection_handler, (void*) clientSocketPtr) < 0) {
			perror("could not create thread");
			exit(EXIT_FAILURE);
		}

		pthread_join(socketHandler, NULL);
	}

	if (clientSocket < 0){
		perror("accept failed");
		exit(EXIT_FAILURE);
	}

	return 0;
}

void *connection_handler(void *socketPtr) {
	// get the socket descriptor
	int socket = *(int*) socketPtr;
	int read_size;
	char clientReq[REQ_LEN];
	char *cmds, *command, *arg;
	
	// receive a request from client
	while((read_size = recv(socket, clientReq, REQ_LEN, 0)) > 0) {
		cmds = trim_whitespace(clientReq);
		command_handler(socket, cmds);
		memset(clientReq, 0, sizeof(clientReq));
	}
	
	if(read_size == 0) {
		puts("client disconnected");
		fflush(stdout);
	} else if(read_size == -1) {
		perror("recv failed");
	}
		
	// free the socket pointer
	free(socketPtr);
	return 0;
}

void command_handler(int socket, char *cmd){
	if(startsWith("login", cmd)){
		removeSubstr(cmd, "login");
		cmd = trim_whitespace(cmd);
		login_user(socket, cmd);
	}
	else if (startsWith("CREATE USER", cmd)){
		removeSubstr(cmd, "CREATE USER");
		cmd = trim_whitespace(cmd);
		register_user(socket, cmd);
	}
}

bool startsWith(const char *pre, const char *str) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

int register_user(int socket, char *arg){
	char *username, *password, *response;
	username = strtok(arg, " ,.-");
	password = strtok(NULL, "");

	int check_user, check_pass;
	check_user = valid_user(username, strlen(username));
	check_pass = valid_pass(password, strlen(password));

	if(check_user){
		response = "username has already been taken\n";
		write(socket, response , strlen(response));
		return EXIT_FAILURE;
	} 

	if(check_pass){
		response = "password must contain atleast 6 character, a number, a lowercase, and an uppercase\n";
		write(socket, response , strlen(response));
		return EXIT_FAILURE;
	} 

	write_user_info(username, password);
	response = "user registered succesfully\n";
	write(socket, response , strlen(response));
	return EXIT_SUCCESS;
}

int login_user(int socket, char *arg){
	char *username, *password, *response;
	username = strtok(arg, " ,.-");
	password = strtok(NULL, "");

	if(login_check(username, password)) {
		response = "logged in successfully";
		write(socket, response , strlen(response));
		return EXIT_SUCCESS;
	} else {
		response = "wrong credentials";
		write(socket, response , strlen(response));
		return EXIT_FAILURE;
	}
}

char *trim_whitespace(char *str){
	char *end;
	while(isspace((unsigned char)*str)) str++;
	if(*str == 0) return str;
	end = str + strlen(str) - 1;
	while(end > str && isspace((unsigned char)*end)) end--;
	
	// add null arnold schwarzenegger
	end[1] = '\0';
	return str;
}

int write_user_info(char *user, char *pass){
	FILE* file = fopen("./databases/user.txt", "a");
	fprintf(file,"%s:%s\n", user, pass);
	
    fclose(file);
    return EXIT_SUCCESS;
}

int valid_user(char* user, int len){
	FILE* file = fopen("./databases/user.txt", "r");
	if(!file) return EXIT_SUCCESS;
	
	char line[100];
	while(fgets(line, sizeof(line), file)){
		printf("h0\n");
		if(strncmp(line, user, len) == 0) {
			printf("uh oh");
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

int valid_pass(char* pass, int len){
	if(len < 6)	return EXIT_FAILURE;
	int low = 0,up = 0, dig = 0;
	for(int i = 0; i < len; i++){
		if(isdigit(pass[i])) dig = 1;
		if(isupper(pass[i])) up = 1;
		if(islower(pass[i])) low = 1;
	}
	
    if((up + dig + low) == 3) return EXIT_SUCCESS;
	else EXIT_FAILURE;
}

int login_check(char* user, char* pass) {
	FILE* file = fopen("./databases/user.txt", "r");
	if(!file) return EXIT_FAILURE;

	char line[100], *username, *password;
	while(fgets(line, sizeof(line), file)) {
		username = strtok(line, ":");
		password = strtok(NULL, "");

		if(strcmp(user, username) == 0 &&
		   strcmp(pass, password) == 0)
		   return EXIT_SUCCESS;
	}
	
	return EXIT_FAILURE;
}

void removeSubstr (char *string, char *sub) {
    char *match = string;
    int len = strlen(sub);
    while ((match = strstr(match, sub))) {
        *match = '\0';
        strcat(string, match+len);
                match++;
    }
}